defmodule Tietopaketti.ProcDiskstats do
  import Tietopaketti.TypedStruct

  @fields 17

  deftypedstruct(%{
    block: String.t(),
    reads_completed: {non_neg_integer(), 0},
    reads_merged: {non_neg_integer(), 0},
    sectors_read: {non_neg_integer(), 0},
    milliseconds_reading: {non_neg_integer(), 0},
    writes_completed: {non_neg_integer(), 0},
    writes_merged: {non_neg_integer(), 0},
    sectors_written: {non_neg_integer(), 0},
    milliseconds_writing: {non_neg_integer(), 0},
    ios_in_progress: {non_neg_integer(), 0},
    milliseconds_doing_io: {non_neg_integer(), 0},
    weighted_milliseconds_doing_io: {non_neg_integer(), 0},
    discards_completed: {non_neg_integer(), 0},
    discards_merged: {non_neg_integer(), 0},
    sectors_discarded: {non_neg_integer(), 0},
    milliseconds_discarding: {non_neg_integer(), 0},
    flush_requests_completed: {non_neg_integer(), 0},
    milliseconds_flushing: {non_neg_integer(), 0}
  })

  def sector_bytes(), do: 512

  def path(), do: "/proc/diskstats"

  def parse_line(line) do
    parts = line |> String.split(" ") |> Enum.reject(&(&1 == "")) |> Enum.with_index()

    [_, _, {block, _} | parts] = parts

    for {part, i} <- parts, reduce: %__MODULE__{block: block} do
      acc ->
        val =
          case Integer.parse(part) do
            {int, _bin} -> int
            :error -> 0
          end

        if i <= @fields - 1 + 3 do
          key =
            case i do
              3 -> :reads_completed
              4 -> :reads_merged
              5 -> :sectors_read
              6 -> :milliseconds_readin
              7 -> :writes_completed
              8 -> :writes_merged
              9 -> :sectors_written
              10 -> :milliseconds_writin
              11 -> :ios_in_progres
              12 -> :milliseconds_doing_io
              13 -> :weighted_milliseconds_doing_io
              14 -> :discards_completed
              15 -> :discards_merged
              16 -> :sectors_discarded
              17 -> :milliseconds_discarding
              18 -> :flush_requests_completed
              19 -> :milliseconds_flushing
            end

          Map.put(acc, key, val)
        else
          acc
        end
    end
  end
end
