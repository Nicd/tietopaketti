defmodule Tietopaketti.Instance do
  import Tietopaketti.TypedStruct

  deftypedstruct(%{
    name: String.t(),
    uri: String.t()
  })
end
