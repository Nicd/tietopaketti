defmodule Tietopaketti.Sysmon do
  use GenServer
  import Tietopaketti.TypedStruct
  alias Tietopaketti.Sysdata
  alias Tietopaketti.ProcDiskstats

  defmodule Options do
    deftypedstruct(%{
      name: GenServer.name(),
      pubsub_name: GenServer.name(),
      update_interval: non_neg_integer(),
      disks: %{optional(String.t()) => String.t()}
    })
  end

  defmodule State do
    deftypedstruct(%{
      pubsub_name: GenServer.name(),
      update_interval: non_neg_integer(),
      paths_by_block: %{optional(String.t()) => String.t()},
      blocks_by_path: %{optional(String.t()) => String.t()},
      prev_disk_stats: %{optional(String.t()) => ProcDiskstats.t()}
    })
  end

  def pubsub_topic(), do: "tietopaketti:sysmon"

  @spec start_link(Options.t()) :: GenServer.on_start()
  def start_link(%Options{} = opts) do
    GenServer.start_link(__MODULE__, opts, name: opts.name)
  end

  @impl GenServer
  @spec init(Options.t()) :: {:ok, State.t()}
  def init(%Options{} = opts) do
    Process.send_after(self(), :update, opts.update_interval)

    {:ok,
     %State{
       pubsub_name: opts.pubsub_name,
       update_interval: opts.update_interval,
       paths_by_block: opts.disks,
       blocks_by_path: Map.new(opts.disks, fn {k, v} -> {v, k} end),
       prev_disk_stats: Map.new(opts.disks, fn {k, _v} -> {k, %ProcDiskstats{block: k}} end)
     }}
  end

  @impl GenServer
  def handle_info(:update, %State{} = state) do
    state = do_update(state)
    Process.send_after(self(), :update, state.update_interval)
    {:noreply, state}
  end

  defp do_update(%State{} = state) do
    cpu =
      :cpu_sup.util([:per_cpu])
      |> Enum.map(fn {_i, usage, _idle, _misc} -> usage end)

    mem_data = :memsup.get_system_memory_data()

    disk_data = :disksup.get_disk_data()
    disk_activity = load_disk_activity(state)

    disk_data =
      for {{path, total, used}, i} <- Enum.with_index(disk_data),
          path = to_string(path),
          Map.has_key?(state.blocks_by_path, path) do
        block = Map.fetch!(state.blocks_by_path, path)

        %Sysdata.Disk{
          id: i,
          path: path,
          total: total,
          used_percent: used,
          stats: Map.get(disk_activity, block, %ProcDiskstats{block: block}),
          prev_stats: Map.get(state.prev_disk_stats, block, %ProcDiskstats{block: block})
        }
      end

    free_memory =
      case Keyword.get(mem_data, :available_memory, :unavailable) do
        :unavailable ->
          Keyword.get(mem_data, :free_memory, 0.0) + Keyword.get(mem_data, :buffered_memory, 0.0) +
            Keyword.get(mem_data, :cached_memory, 0.0)

        value ->
          value
      end

    mem = %Sysdata.Mem{
      total_ram: Keyword.get(mem_data, :system_total_memory, 0.0),
      free_ram: free_memory,
      total_swap: Keyword.get(mem_data, :total_swap, 0.0),
      free_swap: Keyword.get(mem_data, :free_swap, 0.0)
    }

    data = %Sysdata{
      cpu: cpu,
      mem: mem,
      disk: disk_data
    }

    Phoenix.PubSub.broadcast(state.pubsub_name, pubsub_topic(), {Tietopaketti.Sysdata, data})
    %State{state | prev_disk_stats: disk_activity}
  end

  defp load_disk_activity(%State{} = state) do
    with {:ok, content} <- File.read(ProcDiskstats.path()),
         lines <- String.split(content, "\n") do
      for line <- lines,
          line != "",
          parsed = ProcDiskstats.parse_line(line),
          Map.has_key?(state.paths_by_block, parsed.block),
          reduce: %{} do
        acc -> Map.put(acc, parsed.block, parsed)
      end
    else
      _ -> %{}
    end
  end
end
