defmodule Tietopaketti.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the PubSub system
      {Phoenix.PubSub, name: Tietopaketti.PubSub},
      {Tietopaketti.Sysmon,
       %Tietopaketti.Sysmon.Options{
         name: Tietopaketti.Sysmon,
         pubsub_name: Tietopaketti.PubSub,
         update_interval: Application.fetch_env!(:tietopaketti, :update_interval),
         disks: Application.fetch_env!(:tietopaketti, :disks)
       }},
      # Start the Endpoint (http/https)
      TietopakettiWeb.Endpoint
      # Start a worker by calling: Tietopaketti.Worker.start_link(arg)
      # {Tietopaketti.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Tietopaketti.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    TietopakettiWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
