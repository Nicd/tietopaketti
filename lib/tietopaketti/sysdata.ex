defmodule Tietopaketti.Sysdata do
  import Tietopaketti.TypedStruct

  defmodule Disk do
    deftypedstruct(%{
      id: non_neg_integer(),
      path: String.t(),
      total: non_neg_integer(),
      used_percent: 0..100,
      stats: Tietopaketti.ProcDiskstats.t(),
      prev_stats: Tietopaketti.ProcDiskstats.t()
    })
  end

  defmodule Mem do
    deftypedstruct(%{
      total_ram: non_neg_integer(),
      free_ram: non_neg_integer(),
      total_swap: non_neg_integer(),
      free_swap: non_neg_integer()
    })
  end

  deftypedstruct(%{
    cpu: [Float.t()],
    mem: Mem.t(),
    disk: [Disk.t()]
  })
end
