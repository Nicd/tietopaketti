defmodule TietopakettiWeb.Layouts do
  use TietopakettiWeb, :html

  embed_templates "layouts/*"
end
