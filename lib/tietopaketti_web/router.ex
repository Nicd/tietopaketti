defmodule TietopakettiWeb.Router do
  use TietopakettiWeb, :router

  pipeline :browser do
    plug TietopakettiWeb.BaseConfigPlug
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {TietopakettiWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", TietopakettiWeb do
    pipe_through :browser

    live "/", DashLive.Index
  end
end
