defmodule TietopakettiWeb.DashLive.Index do
  use TietopakettiWeb, :live_view
  alias Tietopaketti.ProcDiskstats

  def mount(_params, _session, socket) do
    socket =
      assign(socket, instance: Application.fetch_env!(:tietopaketti, :instance), sysdata: nil, counter: 0)

    if connected?(socket) do
      Phoenix.PubSub.subscribe(Tietopaketti.PubSub, Tietopaketti.Sysmon.pubsub_topic())
    end

    {:ok, socket}
  end

  def handle_info(msg, socket)

  def handle_info({Tietopaketti.Sysdata, sysdata}, socket) do
    {:noreply, assign(socket, sysdata: sysdata, counter: socket.assigns.counter + 1)}
  end

  def handle_info(_msg, socket) do
    {:noreply, socket}
  end

  def reads_getter(%ProcDiskstats{} = stats), do: stats.sectors_read
  def writes_getter(%ProcDiskstats{} = stats), do: stats.sectors_written

  def sector_diff_bytes(prev_stats, stats, getter) do
    prev = getter.(prev_stats)
    now = getter.(stats)

    (now - prev) * ProcDiskstats.sector_bytes()
  end

  def humanize_size(bytes, add_unit) do
    bytes = bytes + 0.0

    {size, unit} =
      cond do
        bytes >= 1024 ** 3 -> {bytes / 1024 ** 3, "GiB"}
        bytes >= 1024 ** 2 -> {bytes / 1024 ** 2, "MiB"}
        bytes >= 1024 -> {bytes / 1024, "KiB"}
        bytes -> {bytes, "B"}
      end

    "#{Float.round(size, 2)}" <>
      if add_unit, do: " #{unit}", else: ""
  end

  def humanize_size_si(bytes, add_unit) do
    bytes = bytes + 0.0

    {size, unit} =
      cond do
        bytes >= 1000 ** 3 -> {bytes / 1000 ** 3, "GB"}
        bytes >= 1000 ** 2 -> {bytes / 1000 ** 2, "MB"}
        bytes >= 1000 -> {bytes / 1000, "kB"}
        bytes -> {bytes, "B"}
      end

    "#{Float.round(size, 2)}" <>
      if add_unit, do: " #{unit}", else: ""
  end
end
