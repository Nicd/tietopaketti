defmodule TietopakettiWeb.Progress do
  use TietopakettiWeb, :live_component

  @max_history 100

  def mount(socket) do
    {:ok,
     assign(socket,
       label: "",
       id: "",
       value_display: "",
       vals: 1..@max_history |> Enum.map(fn _ -> 0 end) |> :queue.from_list(),
       max: 0
     )}
  end

  def update(assigns, socket) do
    vals = :queue.drop(:queue.in(assigns.value, socket.assigns.vals))

    as_list = :queue.to_list(vals)
    max = Map.get(assigns, :max, Enum.max(as_list))

    {:ok,
     socket
     |> assign(
       label: assigns.label,
       id: assigns.id,
       value_display: assigns[:"value-display"],
       vals: vals,
       vals_as_list: as_list,
       max: max
     )}
  end

  def render(assigns) do
    ~H"""
    <div id={@id} class="progress">
      <label for={@id}><%= @label %></label>
      <div class="progress-current">
        <%= @value_display %>
      </div>
      <div id={"#{@id}-vals"} class="progress-vals" aria-hidden="true">
        <%= for {val, i} <- Enum.with_index(@vals_as_list), @max > 0 do %>
          <div
            id={"#{@id}-vals-#{i}"}
            class="progress-val"
            style={"height:#{Float.round(val / @max * 100)}%;"}
          >
          </div>
        <% end %>
      </div>
    </div>
    """
  end
end
