defmodule TietopakettiWeb.BaseConfigPlug do
  @behaviour Plug

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  def call(conn, _opts) do
    Plug.Conn.assign(conn, :instance, Application.fetch_env!(:tietopaketti, :instance))
  end
end
